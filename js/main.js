$(document).ready(function () {
  
  $('#exampleModalCenter').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
  })

  $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
  })

  ko.applyBindings(m_site);
  ko.validation.init();

  var cart = JSON.parse(localStorage.getItem('cart'))
  if (cart) m_cart.cartList(cart) 
  
})