//
ko.bindingHandlers.initToggle = {
    init: function (element) {
      $(element).tooltip();
    }
  };

ko.bindingHandlers.initCarousel = {

  update: function (element, valueAccessor) {
    setTimeout(function(){
      var value = ko.utils.unwrapObservable(valueAccessor()); // Get the current value of the current property we're bound to
    
    
    $(element).slick({
      slidesToShow: 4,
      infinite: false,
      slidesToScroll: 1,
    })
  },1000)
  }
}



ko.bindingHandlers.initProductCarousel = {
  init: function (element) {
    setTimeout(function(){
    $(element).find('.initProductCarouselMain').slick({
      slidesToShow:1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.initProductCarouselDown'
    });
    $(element).find('.initProductCarouselDown').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.initProductCarouselMain',
      dots: true,
      centerMode: true,
      focusOnSelect: true
    });
  },0)
}}