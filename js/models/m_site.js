m_site = {
    listItems: ko.observableArray([
      {
        id: 0,
        name:'Рубашка',
        price: 1260,
        sizes: ['S','M','XL'],
        colors:['skyblue','grey','crimson'],
        description: 'Хлопковая рубашка качества premium. Zombie ipsum reversus ab viral inferno, nam rick grimes malum',
        photos:['https://images.pexels.com/photos/297933/pexels-photo-297933.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940','https://images.asos-media.com/products/chernaya-rubashka-v-melkuyu-lomanuyu-kletku-farah-buckley/12651430-1-black?$XXL$&wid=513&fit=constrain',
      'https://images.asos-media.com/products/serovato-bezhevaya-rubashka-navypusk-v-kletku-asos-design/12753760-1-beige?$n_320w$&wid=317&fit=constrain',
      'https://images.asos-media.com/products/rozovaya-rubashka-navypusk-s-vorotnikom-na-pugovitse-asos-design/11599115-1-pink?$n_320w$&wid=317&fit=constrain',
      'https://images.asos-media.com/products/krasnaya-rubashka-v-vintazhnom-stile-asos-design/10559141-1-red?$n_320w$&wid=317&fit=constrain',
      'https://images.asos-media.com/products/krasno-buraya-rubashka-navypusk-s-kontrastnoj-strochkoj-asos-design/10634664-1-rust?$n_320w$&wid=317&fit=constrain',
      'https://images.asos-media.com/products/rubashka-navypusk-verblyuzhego-tsveta-v-stile-militari-fred-perry/10462553-1-tan?$n_320w$&wid=317&fit=constrain' ],
        oldPrice: 1800,
        discount: 'Скидка 30%',
      },

      {
        id: 1,
        name:'Брюки',
        price: 1200,
        sizes: ['S','M','L'],
        colors:['Белый','Черный','Желтый'],
        description: 'Брюки колюты b viral inferno, nam rick grimes malum cerebro.',
        photos:['https://images.asos-media.com/products/bryuki-v-stile-militari-ryzhego-tsveta-s-poyasom-naanaa/12602239-1-rust?$n_320w$&wid=317&fit=constrain','https://images.pexels.com/photos/914668/pexels-photo-914668.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'],
        oldPrice: 1800,
        discount: 'Скидка 30%',
      },

      {        
        id: 2,
        name:'Джинсовка',
        price: 1600,
        sizes: ['S','M','L'],
        colors:['Белый','Черный','Желтый'],
        description: 'Брюки колюты b viral inferno, nam rick grimes malum cerebro.',
        photos:['https://images.asos-media.com/products/sinyaya-vybelennaya-dzhinsovaya-oversize-kurtka-liquor-n-poker/12999596-1-stonewashblue?$XXL$&wid=513&fit=constrain','https://images.pexels.com/photos/1176618/pexels-photo-1176618.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'],
        oldPrice: 2000,
        discount: 'Скидка 20%',
      },

      {
        id: 3,
        name:'Брюки',
        price: 1200,
        sizes: ['S','M','L'],
        colors:['red','blue','white'],
        description: 'Брюки колюты b viral inferno, nam rick grimes malum cerebro.',
        photos:['https://images.asos-media.com/products/bryuki-v-stile-militari-ryzhego-tsveta-s-poyasom-naanaa/12602239-1-rust?$n_320w$&wid=317&fit=constrain','https://images.pexels.com/photos/914668/pexels-photo-914668.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'],
        oldPrice: 1800,
        discount: 'Скидка 30%',
      },

      {
        id: 4,
        name:'Брюки',
        price: 1200,
        sizes: ['S','M','L'],
        colors:['red','blue','white'],
        description: 'Брюки колюты b viral inferno, nam rick grimes malum cerebro.',
        photos:['https://images.pexels.com/photos/297933/pexels-photo-297933.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940','https://images.asos-media.com/products/chernaya-rubashka-v-melkuyu-lomanuyu-kletku-farah-buckley/12651430-1-black?$XXL$&wid=513&fit=constrain'],
        oldPrice: 1800,
        discount: 'Скидка 30%',
      },

      {
        id: 5,
        name:'Брюки',
        price: 1200,
        sizes: ['S','M','L'],
        colors:['Белый','Черный','Желтый'],
        description: 'Брюки колюты b viral inferno, nam rick grimes malum cerebro.',
        photos:['https://images.asos-media.com/products/bryuki-v-stile-militari-ryzhego-tsveta-s-poyasom-naanaa/12602239-1-rust?$n_320w$&wid=317&fit=constrain','https://images.pexels.com/photos/914668/pexels-photo-914668.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'],
        oldPrice: 1800,
        discount: 'Скидка 30%',
      },

    ]),


    
    counter: ko.observable(1),
    choosenSize: ko.observable(),
    choosenColor: ko.observable(),


  };
 
  m_site.changeCounter = function(increase) {
    // console.log('increase', increase);
    increase ? m_site.counter(Number(m_site.counter()) + 1) : m_site.counter(m_site.counter() - 1)
  }

  m_site.isDisabled = function () {    
    m_site.counter() > 15 ? false : true
  }

  m_site.itemToCart= function(el, e, id = 0) {  
    if(m_cart.cartList().length) {
      var sameElement = m_cart.cartList().find((element) => {
        return element.id === id && element.colorCart === m_site.choosenColor() && element.sizeCart === m_site.choosenSize()
      })
      
      if (sameElement) {
        var tempEl = sameElement        
        tempEl.countCart = sameElement.countCart + m_site.counter() < 16 ?
        m_cart.cartList.replace(sameElement, tempEl) : alert('Мы столько не можем!')
        console.log(m_cart.cartList())
      } else {
        var newItem = {
          id: id,
          nameCart: m_site.listItems()[0].name,
          descrCart: m_site.listItems()[0].description,
          colorCart: m_site.choosenColor(),
          sizeCart: m_site.choosenSize(),
          countCart: m_site.counter(),
          photo: m_site.listItems()[0].photos[0],
          priceCart: m_site.listItems()[0].price,
        };
  
        m_cart.cartList.push(newItem);
        console.log(m_cart.cartList())
      }
    } else {
      console.log(id)
      var newItem = {
        id: id,
        nameCart: m_site.listItems()[0].name,
        descrCart: m_site.listItems()[0].description,
        colorCart: m_site.choosenColor(),
        sizeCart: m_site.choosenSize(),
        countCart: m_site.counter(),
        photo: m_site.listItems()[0].photos[0],
        priceCart: m_site.listItems()[0].price,
      };

      m_cart.cartList.push(newItem);
      console.log(m_cart.cartList())
    }
  }
 

  class Item{

    constructor(params) {

      console.log("params",params)

      this.name=params.name||"_default_"
    // name:'Брюки',
    // price: 1200,
    // sizes: ['S','M','L'],
    // colors:['Белый','Черный','Желтый'],
    // description: 'Брюки колюты b viral inferno, nam rick grimes malum cerebro.',
    // photos:['https://images.pexels.com/photos/297933/pexels-photo-297933.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940','https://images.asos-media.com/products/chernaya-rubashka-v-melkuyu-lomanuyu-kletku-farah-buckley/12651430-1-black?$XXL$&wid=513&fit=constrain'],
    // oldPrice: 1800,
    // discount: 'Ск
    }
  }