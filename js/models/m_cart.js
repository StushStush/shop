m_cart = {    
    tempCloth: ko.observable(''),
    tempSize: ko.observable(''),
    tempColor: ko.observable(''),
    cartList: ko.observableArray([]),
    quantityItem: ko.observable('').extend({
      required: true,
      number: true,                
      maxLength: {
        params: 2,
        message: 'Please, less than 60 :('
      },               
    }),
  
}

m_cart.restrict = function(min,max, name, data, event) {
    const val =  event.target.value;    
      if (val<max && val>min) data[name](val)
      else if (val<min) return data[name](min); 
      else if (val>max) return data[name](max);
      else return 0;
  }

  m_cart.addItem = function () {
    var newItem = {
      name: m_cart.tempCloth().name,
      price: m_cart.tempCloth().price,
      color: m_cart.tempColor(),
      size: m_cart.tempSize(),
      quantityOf: m_cart.quantityItem(),
      img: m_cart.tempCloth().img,
    }
      m_cart.cartList.push(newItem);  
  }
  
 
  m_cart.cartList.subscribe(function(){
    localStorage.setItem('cart', JSON.stringify(m_cart.cartList()));
  })
  
  m_cart.summm = ko.computed(function () {
    var total = 0;
    m_cart.cartList().forEach(function (element) {
      total += (element.priceCart * element.countCart);
    });
    return total + ' ' + 'руб.';
  })

  m_cart.isEmptyCart = function () {
    return m_cart.cartList().length === 0 ? true : false
  }

  m_cart.removeItem = function () {
    m_cart.cartList.remove(this)
  }

  m_cart.itemTotal = ko.computed(function () {
    return m_cart.tempCloth().price * m_cart.quantityItem() + ' ' + 'руб.'
  })

  